import Box from './Class/Box.js'
import Game from './Class/Game.js'


// CREATE NEW BOX AND PUSH IT IN BOXS ARRAY
let boxs = []
boxs.push(
    new Box("Fermiers", "icones/fermier.png", 10, 10, 1, 6000, 1),
    new Box("Tracteurs", "icones/tracteur.png", 25, 25, 2, 10000),
    new Box("Hydroponie", "icones/hydroponique.png", 100, 100, 3, 16000),
    new Box("Industrie", "icones/industrie.png", 10000, 1000, 4, 20000),
)


// CREATE NEW GAME
const GameBox = new Game(document.getElementById("game"), boxs)
GameBox.refresh()
GameBox.appendBoxs()


// BUTTONS ONCLICK LISTENER
for (let i = 0; i < boxs.length; i++) {

    // BUYING WORKER
    boxs[i].html.button.addEventListener("click", (e) => {
        boxs[i].addWorker(e.target.textContent)
        GameBox.people -= Number(e.target.textContent)
        GameBox.result -= Number(e.target.textContent) * boxs[i].workerPrice
        GameBox.refresh()
    })


    // FASTER BUILDING
    boxs[i].html.faster_btn.addEventListener("click", (e) => {
        boxs[i].html.faster_btn.classList.add("hidden")
        console.log(boxs[i].duration);
        boxs[i].changeSpeed(boxs[i].duration)
        console.log(boxs[i].duration);
        boxs[i].hasUp = true
    })


    console.log("IN GAME : ", boxs[i].duration, boxs[i].hasUp);

    // WORK
    GameBox.working(boxs[i])
    // setInterval(() => {

    //     // Resultat = valeur * travailleur
    //     GameBox.result += boxs[i].gainValue * boxs[i].worker

    //     // Affichage des infos
    //     GameBox.refresh()

    // }, boxs[i].duration);
}