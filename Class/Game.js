export default class Game {

    /**
     * 
     * @param {DomElement} gameDiv the element where display the game 
     * @param {Array} boxs Array of game-boxes 
     */
    constructor(gameDiv, boxs = [],) {
        this.gameDiv = gameDiv
        this.boxs = boxs
        this.people = 0
        this.result = 0
        this.peopleDuration = 2000
        this.header = this.appendHead()
        // On augmente le nombre de personne
        this.runGame = setInterval(() => {
            this.people++
            this.header.peopleTxt.textContent = this.people
            this.refresh()
        }, this.peopleDuration)
    }


    /**
     * CREATE THE HEADER COUNT BOX
     * @returns {HtmlElement} peopleTxt resultTxt; the elements of the dom content for people and result values
     */
    appendHead() {
        let head = document.createElement("div");
        head.className = "head"

        let info = document.createElement("div")
        info.className = "info-head"
        let p = document.createElement("p");
        p.textContent = "Peoples"
        let peopleTxt = document.createElement("h2");

        info.append(p, peopleTxt)

        let bar = document.createElement("div")
        bar.className = "progress-bar"
        let bar_fill = document.createElement("span")
        bar_fill.id = "bar_people_fill"
        bar_fill.className = "bar-people-fill"

        let div = document.createElement("div")
        let p2 = document.createElement("p")
        p2.textContent = "Ressources"
        let resultTxt = document.createElement("h4");

        div.append(p2, resultTxt)
        bar.append(bar_fill, div)
        head.append(info, bar)

        document.body.insertAdjacentElement("beforebegin", head)

        return {
            peopleTxt,
            resultTxt
        }
    }


    /**
     * REFRESH DATAS ON SCREEN
     */
    refresh() {
        this.boxs.forEach(box => {
            box.html.worker.textContent = box.worker
            box.html.value.textContent = box.gainValue * box.worker

            let buy = Math.floor(this.result / box.workerPrice)

            if (buy > this.people) {
                box.html.button.textContent = this.people;
            } else {
                box.html.button.textContent = buy;
            }

            this.header.resultTxt.textContent = this.result
            this.header.peopleTxt.textContent = this.people

            if (box.worker >= 10 && !box.hasUp) {

                let css = "font-weight:bold;font-size:1.25rem;color:dodgerblue;"
                console.log("%cGET IN", css, box.name, box.html.bar.style.animationDuration);

                box.html.faster_btn.classList.remove("hidden")
                // box.changeSpeed(box.duration)
                // Up done
                box.hasUp = true
            }
        });
    }


    /**
     * DISPLAYED THE BOXS INTO GAMEDIV
     */
    appendBoxs() {
        this.boxs.forEach((box) => {
            this.gameDiv.append(box.html.box)
        })
    }


    working(box) {
        setInterval(() => {

            // Resultat = valeur * travailleur
            this.result += box.gainValue * box.worker

            this.refresh()
        }, box.duration);
    }

}
