export default class Box {
    

    /**
     * 
     * @param {String} name Name of the box
     * @param {Number} gainValue Gain value of the worker
     * @param {Number} workerPrice Worker price
     * @param {Number} rank Unique-Id of the box
     * @param {Number} duration Ressource building Duration/Time
     * @param {Number} worker Base number of workers
     */
    constructor(name = "", ico_url = "", gainValue = 0, workerPrice = 0, rank = 0, duration = 6000 , worker = 0) {
        this.name = name
        this.ico_url = ico_url
        this.gainValue = gainValue
        this.workerPrice = workerPrice
        this.duration = duration
        this.worker = worker
        this.rank = rank
        this.html = this.createBox()
        this.hasUp = false
    }


    /**
     * CREATE BOX OBJECT
     * @returns {box, value, worker, button, duration} Html Elements, and Animation-bar duration
     */
    createBox() {

        const box = this.createElements("div", "box")

        const head = this.createElements("div", "box-head")
        const name = this.createElements("div", "name", this.name)
        const price = this.createElements("div", "price", this.workerPrice)
        
        const body = this.createElements("div", "box-body")
        const img = document.createElement("img")
        img.src = this.ico_url

        const value = this.createElements("div", "value", this.gainValue.toString())
        const fasterBtn = this.createElements("button", "faster-btn hidden", "Faster ?", this.name)
        
        const div = this.createElements("div", "img-value")
        const worker = this.createElements("div", "text", this.worker)
        div.append(img, worker)        
        const button = this.createElements("button", "button", "0")

        const bar = this.createElements("div", "progress-bar")
        const bar_prog = this.createElements("span", "progress-bar-fill")
        bar_prog.style.animationDuration = this.getSeconds(this.duration)

        // HEAD BODY BAR
        head.append(name, price)
        body.append(div, value, fasterBtn, button)
        bar.append(bar_prog)

        box.append(head, body, bar)

        return {
            box,
            value,
            worker,
            button,
            faster_btn: fasterBtn,
            bar: bar_prog
        }

    }


    /**
     * CREATE DOM ELEMENT
     * @param {String} type HtmlElements type (div, span, p ...).
     * @param {String} name Name for class and id whrite it whith '-' between words.
     * @param {String} text Textcontent if needed.
     * @returns {HtmlElement} Element html.
     */
    createElements(type = "div", name = "", text = "", dataSet = "") {

        let element = document.createElement(type)
        element.id = name.replace("-", "_") + "_" + this.rank
        element.className = name
        if (text !== "") {
            element.textContent = text
        }
        if (dataSet !== "") {
            element.dataset.box_name = dataSet
        }
        return element;
    }


    /**
     * ADD WORKER
     * @param {Number} value The number of workers added.
     */
    addWorker(value = 0) {
        this.worker += Number(value)
    }


    /**
     * CONVERT MS TO SECOND
     * @param {Number} timeMs Time in milliseconds.
     * @returns 
     */
    getSeconds(timeMs) {
        return ((timeMs % 60000) / 1000).toFixed(4) + "s"
    }


    /**
     * CHANGE BUILDING SPEED
     * @param {Number} duration 
     */
    changeSpeed(duration) {
        this.duration = duration / 2
        this.html.bar.style.animationDuration = this.getSeconds(this.duration)
        this.hasUp = false
        console.log("changeSpeed : ", this.duration, this.hasUp);
    }
}
